package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		super();
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}


	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	public void altaCliente(Cliente nuevoCliente) {
	
	}
	
	public void eliminarCliente(String dni) {
		
	}
	
	public void crearFactura(Factura factura) {
		
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
		
	}
	
	public Cliente clienteMasAntiguo() {
		
		return null;
		
	}
	
	
	public Factura buscarFactura(String codigo) {
		
		return null;
		
	}
	
	
	
	public void eliminarFactura(String codigo) {
		
		
	}
	
	
	public Factura facturaMasCara() {
		
		return null;
		
	}
	
	public float calcularFacturacionAnual(int anio) {
		
		return anio;
	}
	
	
	
	public int cantidadFacturasPorCliente(String dni) {
		
		return 0;
		
	}
	
	
	
	public Cliente buscarCliente(String dni) {
		
		
		return null;
	}

	//Metodo  para facilitar las pruebas
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
}
	
	
}