package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class Pruebas_Eliminar {
	static GestorContabilidad gestorPruebas;
	static GestorContabilidad gestorPruebas2;
	static Cliente vCliente;
	static Factura vFactura;

	// Eliminar una factura existente
	@Test
	public void eliminarFacturaExistente() {
		gestorPruebas.getListaFacturas().clear();
		gestorPruebas.getListaFacturas().add(vFactura);
		gestorPruebas.eliminarFactura("12345");
		boolean actual= gestorPruebas2.getListaFacturas().contains(vFactura);
		
		assertFalse(actual);
	}
	
	@Test
	public void eliminarFacturaInexistente() {
		boolean actual = gestorPruebas.getListaFacturas().contains(vFactura);
		assertFalse(actual);
	}
	
	// Eliminar cliente existente
	@Test 
	public void eliminarClienteExistente() {
		gestorPruebas.getListaClientes().clear();
		gestorPruebas.getListaClientes().add(vCliente);
		gestorPruebas.eliminarCliente("1224h");
		boolean encontrado=gestorPruebas2.getListaClientes().contains(vCliente);
		assertFalse(encontrado);
	}
	
	// Eliminar cliente inexistente
	@Test 
	public void eliminarClienteInexistente() {
		boolean encontrado=gestorPruebas2.getListaClientes().contains(vCliente);
		assertFalse(encontrado);
	}
}
