package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class TestClientes {
	static	GestorContabilidad gestorPruebas;
	Cliente actual;
	
	@BeforeClass
	public static void inicializarClase(){
		gestorPruebas = new GestorContabilidad();
	}

	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		//Busco un dni que no exista, debe devolver null
		String dni = "2345234";
		
		Cliente actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteExistente(){
		//busca un cliente que si exista, el resultado debe ser el mismo objeto
		Cliente esperado = new Cliente("1234F", "Fernando", LocalDate.now());
		//A�ado un cliente a la lista de clientes
		gestorPruebas.getListaClientes().add(esperado);

		actual = gestorPruebas.buscarCliente("1234F");

		assertSame(esperado, actual);
	}
	
	@Test
	public void testBuscarClienteInexistenteConClientes(){	
		String dni = "64F";
		//Busco un objeto que no exista, el resultado debe ser null
		actual = gestorPruebas.buscarCliente(dni);	
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteHabiendoVariosClientes(){
		Cliente esperado = new Cliente("34567F", "Maria", LocalDate.parse("1990-05-04"));
		gestorPruebas.getListaClientes().add(esperado);
		String dniABuscar = "1234678L";
		esperado = new Cliente(dniABuscar, "Pedro", LocalDate.parse("1995-07-02"));
		gestorPruebas.getListaClientes().add(esperado);
		
		actual = gestorPruebas.buscarCliente(dniABuscar);
		
		assertSame(esperado, actual);
	}
	

	
}
