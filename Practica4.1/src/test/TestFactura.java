package test;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;

import static java.lang.Math.*;
class FacturasTest {
	static Factura facturaPrueba=new Factura();
	Factura actual;
	
	@BeforeClass
	public static void prepararClasePruebas() {
		facturaPrueba =new Factura();
	}
	
		@Test
		// Test que calcula el precio de un producto a partir de la cantidad y el precio
		 void CalcuclarPrecioProducto() {
			facturaPrueba.setCantidad(2);
			facturaPrueba.setPrecioUnidad(2.5);
			
			double esperado =5;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual);
		}
			
		// Calcular precio negativo
		@Test
		void testCalcularPrecioConPrecioNegativo() {
			facturaPrueba.setCantidad(2);
			facturaPrueba.setPrecioUnidad(-1.5);
			
			double esperado =-3;
			double actual=facturaPrueba.calcularPrecioTotal();
			
			assertEquals(esperado,actual);
			
		}
		//Test que busca una factura
		@Test 
		void buscarFactura() {
			String codigoFactura="2343";
			Factura actual=facturaPrueba.buscarFactura(codigoFactura);
			assertNull(actual);
		}
		//Test que busca una factura ya existente
		@Test 
		void buscarFacturasExistentes() {
			Factura esperado=new Factura("2343",LocalDate.now(),"Portatil",22.5,200, null);
			facturaPrueba.getCodigoFactura().equals(esperado);
			actual = facturaPrueba.buscarFactura("233");
			assertSame(esperado,actual);
			
		}
		
	

}
